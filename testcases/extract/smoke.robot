*** Settings ***
Documentation    Extract Zip
Library          Process

*** Test Cases ***
Extract Zip with Deflate
    Log    Hello, World!

Extract Zip with Deflate64
    Log    Hello, World!

Extract Zip with Bzip2
    Log    Hello, World!
